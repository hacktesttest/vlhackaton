## Vl Hackaton

Команда testtest

##DOCUMENTATION

#ajax views api
using @proj.decorators.ajax takes json-serializable object
from view and wraps it into HttpResponse.

Models inherited from SerializableModel are serializable
Making such model, specify serializable_field list in format:
[<field name>, ..., (<json field name>, <model field name>),,,,]

Modify proj.utils.json_response to add custom serialization handling stuff

@auth.decorators.ajax_login_required checks if request cookie has token
if not, it returns {'error': 'not_authorized'}

@login_required retruns HttpResponseRedirect

