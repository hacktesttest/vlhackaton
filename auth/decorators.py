from django.conf import settings
from django.http import HttpResponseRedirect

from proj.utils import json_response

def ajax_login_required(f):
    def wrp(request, *args, **kwargs):
        if request.vk_user is None:
            return json_response(dict(error='not_authorized'))
        else:
            return f(request, *args, **kwargs)
    return wrp

def login_required(f):
    def wrp(request, *args, **kwargs):
        if request.vk_user is None:
            return HttpResponseRedirect(settings.LOGIN_URL)
        else:
            return f(request, *args, **kwargs)
    return wrp
