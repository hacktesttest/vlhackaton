from django.utils.functional import SimpleLazyObject

from meetfactory.models import User
#import .vk

class VkUserMiddleware(object):
    def process_request(self, request):
        request.vk_user = None
        token = request.COOKIES.get('access_token')
        if not token:
            return None
        #j = vk.api('users.get', access_token=token)
        #if j.get('error'):
        #    return None
        try:
            request.vk_user = SimpleLazyObject(User.objects.get(access_token=token))
            #(vk_id=j['response'][0]['uid'])
        except:
            return None

        return None
