from httplib2 import Http
from urllib import urlencode
import json

VK_API_URI = "https://api.vk.com/method/"

def api(method, **args):
    url = VK_API_URI + method + '?' + urlencode(args)
    resp, data = Http().request(url)
    return json.loads(data)
    
