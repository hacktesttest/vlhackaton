from django.contrib import admin

from meetfactory.models import *

admin.site.register(User)
admin.site.register(Category)
admin.site.register(Tag)
admin.site.register(Event)
admin.site.register(EventUser)
admin.site.register(EventTag)