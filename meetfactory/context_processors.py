from django.conf import settings

def vk(request):
    return dict(VK_APP_ID=settings.VK_APP_ID,
            need_to_login=request.vk_user is None)
