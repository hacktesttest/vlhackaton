# -*- coding: <utf-8> -*-

from django.db import models
from django.forms import ModelForm
from django import forms
from datetime import *
from django.db.models import (ManyToManyField, CharField, IntegerField,
        ForeignKey, DateTimeField, FloatField, TextField, Model)

class SerializableModel(Model):
    class Meta:
        abstract = True

    def serialize(self):
        def getf(f):
            if hasattr(f, '__iter__'):
                f = list(f)
            else:
                f = (f, f,)
            return (f[0], getattr(self, f[1]))
        if hasattr(self, 'serializable_fields'):
            fields = self.serializable_fields
            return dict(map(getf, fields))
        elif hasattr(self, '__unicode__'):
            return self.__unicode()

class User(SerializableModel):
        vk_id = CharField(max_length=100)
        access_token = CharField(max_length=200)
        expires_in = IntegerField(max_length=200)
        first_name = CharField(max_length=200)
        last_name = CharField(max_length=200)
        userpic = CharField(max_length=200)

        serializable_fields = [
            'first_name', 'last_name', 'vk_id',
            ('url_pic', 'userpic',),]

        def __unicode__(self):
            return self.vk_id

class Category(SerializableModel):
        name = CharField(max_length=100)

        def __unicode__(self):
            return unicode(self.name)

class Tag(SerializableModel):
        name = CharField(max_length=10)

        def __unicode__(self):
            return self.name

class Event(SerializableModel):
        name = CharField(max_length=100)
        low_people_count = IntegerField(default=0)
        high_people_count = IntegerField(default=0)
        creator = ForeignKey(User, related_name='created_events')
        begin_date = DateTimeField()
        end_date = DateTimeField()
        money = FloatField(blank=True, default=0)
        desc = TextField()
        location = TextField(blank=True)
        category = ForeignKey(Category, blank=True, default=1)

        tags = ManyToManyField(Tag, through='EventTag')
        users = ManyToManyField(User, through='EventUser')

        serializable_fields = [
            'id', 'name', 'creator',
            'low_people_count',
            'high_people_count',
            'begin_date', 'end_date',
            'money', 'desc', 'location',]          

        def __unicode__(self):
            return self.name

class EventForm(ModelForm):
        begin_date = forms.DateField(widget=forms.DateInput, input_formats=['%Y-%m-%d', '%m/%d/%Y', '%m/%d/%y'], initial=date.today)

        class Meta:
            model = Event
            fields = "__all__"

class EventUser(SerializableModel):
        user = ForeignKey(User)
        event = ForeignKey(Event)

        def __unicode__(self):
            return self.user.vk_id + " " + self.event.name

class EventTag(SerializableModel):
        event = ForeignKey(Event)
        tag = ForeignKey(Tag)

        def __unicode__(self):
            return self.event.name + " " + self.tag.name
