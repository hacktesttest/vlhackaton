/*Создание карты */
var myMap;
//2 массива с шриотой и долготой для передачи в бд
var lon = new Array();
var lat = new Array();
var itt = 0;
//Итератор, исользующийся для нумерации полей списка в DOM
var marker_id = 0;
//Массив улиц для эелментов
var street = new Array();
// Создаем обработчик загрузки страницы:
DG.autoload(function() {
    // Создаем объект карты, связанный с контейнером:
    myMap = new DG.Map('map');
    // Устанавливаем центр карты и коэффициент масштабирования:
    myMap.setCenter(new DG.GeoPoint(131.982190206367,43.106460416169),16);
    // Добавляем элемент управления коэффициентом масштабирования:
    myMap.controls.add(new DG.Controls.Zoom());
});

/* Поиск объектов на карте через input */

$("#geo").on('click' , function(event) { 
    event.preventDefault();
    var query = $("input.geo_info").val();
    console.log(query);
    // Выполнение поиска:
        DG.Geocoder.get(query, {
            types: ['city', 'settlement', 'district', 'street', 'living_area', 'place', 'house', 'station_platform', 'station'],
            limit: 1,
            // Обработка успешного поиска
            success: function(geocoderObjects) {
                // Сброс результатов прошлого поиска:
                    //myMap.markers.removeAll();
                    //myMap.balloons.removeAll();
                // Обходим в цикле все полученные геообъекты:
                for (var i = 0, len = geocoderObjects.length; i < len; i++) {
                    var geocoderObject = geocoderObjects[i];
                    var city = geocoderObject.getAttributes().city;
                    if (city === "Владивосток") {
                        // Получаем необходимые данные о геообъекте и устанавливаем желаемый формат вывода информации
                        var name = geocoderObject.getName();
                        var info = '<b>Имя:</b><br>' + name;
                        info += '<br><a class = "rmv" href = "#">Test</a>';
                        // Опции маркера:
                        var markerOptions = {
                            geoPoint: geocoderObject.getCenterGeoPoint(),
                            // Устанавливаем опции балуна:
                            balloonOptions: {
                                // Указываем содержимое балуна:
                                contentHtml: info
                            }
                        }

                        // Создаем маркер:
                        var marker = new DG.Markers.MarkerWithBalloon(markerOptions);

                        // Добавляем маркер на карту:
                        myMap.markers.add(marker);
                        // Получаем координаты маркера
                        var position = marker.getPosition();
                        //Центрируем карту на найденной позиции
                        myMap.setCenter(position, 17);
                        
                        var flag_lat = searchArray(lat, position.lat);
                        var flag_lon = searchArray(lon, position.lon);
                        var street_tmp = geocoderObject.getAttributes().street;
                        marker_id = marker.getId();
                        //Создаем элемент списка
                        //Проверка на наличие данного элемента в списке, функция из functions.js
                        if (flag_lat && flag_lon) {
                            $("ul").append('<li id = '+marker_id+'><a class = "items" id = '+marker_id+' href="#">'+ name +'</a>     <a class = "del" href = "#" id = '+marker_id+'>Удалить</a></li>');
                            lat.push(position.lat);
                            lon.push(position.lon);
                            street.push(street_tmp);
                            var ss = '<input name = "location'+itt+'" type = "text" value = '+position.lat+';'+position.lon+';'+street_tmp+' />'
                            $("form").append(ss);
                            $("#cnt").val(itt);
                            itt++;
                            };
                        console.log(street);
                        myMap.zoomOut();
                        myMap.zoomIn()
                        
                        //УДаление маркера через элемент списка
                            $("li a.del").on('click', function(event) {
                                event.preventDefault();
                                //Убираем маркер
                                myMap.markers.remove($(this).attr('id'));
                                //Удаляем элемент из массива lat и lon
                                var id_li = $('li').index(document.getElementById($(this).attr('id')));
                                lat.splice(id_li, 1);
                                lon.splice(id_li, 1);
                                street.splice(id_li, 1)
                                //Удаляем элемент li из DOM
                                $(this).parent().remove();
                                console.log(street);
                            });

                        
                    }
                }
            },
            // Обработка ошибок поиска:
            failure: function(code, message) {
                if (code == 404) {
                    alert('Такого адреса не существует.')
                };

                if (code == 500) {
                    alert('Ошибка сервера. Повторите запрос позже')
                };
            }
    });
})