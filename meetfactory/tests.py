from django.test import TestCase
from meetfactory.models import *
from django.core.urlresolvers import reverse
from django.test.client import Client

class JsonTest(TestCase):

	def test_last_events(self):	
		client = Client()
		response = client.post(reverse('meetfactory:json'), 
			{'action':'last_events', 'offset':0})
		print response