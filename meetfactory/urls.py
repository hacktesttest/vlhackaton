from django.conf.urls import patterns, url

from meetfactory import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^json/$', views.jsons, name='json'),
    url(r'^vk_login/$', views.vk_login, name='vk_login'),
    url(r'^event_create/$', views.event_create, name='event_create'),
    url(r'^profile/$', views.profile, name='profile'),
    url(r'^events/add/$', views.subscribe_event, name='add'),
    url(r'^events/$', views.event)
)
