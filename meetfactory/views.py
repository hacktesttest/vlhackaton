import json, datetime, socket
from urllib import urlencode
from httplib2 import Http

from django.shortcuts import render, get_object_or_404, HttpResponseRedirect, HttpResponse, redirect
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.db.models import Q

from meetfactory.models import *
from auth import vk
from proj.decorators import ajax
from auth.decorators import login_required

def index(request):
    events = Event.objects.all()
    q = request.GET.get('q')
    _get = request.GET.get
    G = {f+'q': _get(f+'q') == 'on' for f in 'tdnl'}
    if q:
        N = Q(id=0)
        qtags = Tag.objects.filter(name__contains=q)
        e_tags = Q(tags__in=qtags) if G['tq'] else N
        d_tags = Q(desc__contains=q) if G['dq'] else N
        n_tags = Q(name__contains=q) if G['nq'] else N
        l_tags = Q(location__contains=q) if G['lq'] else N
        events = events.filter(
                e_tags | d_tags | n_tags | l_tags)
    cat = request.GET.get('category')
    if cat:
        events = events.filter(category=cat)
    cs = Category.objects.all()
    need_to_login = request.vk_user is None
    return render(request, 'meets/index.html',
            {'el':events, 'need_to_login':need_to_login,
                'categories':cs, 'cat': cat, 'G': G,'q':q,})


def event(request):
    e = get_object_or_404(Event, pk=request.GET.get('id'))
    tags = ['#' + unicode(t) for t in e.tags.all()]
    u = request.vk_user
    user_in = u is not None and e.users.filter(user=u)
    return render(request, 'meets/event_profile.html', 
            {'e': e,'tags': ' '.join(tags)})

@login_required
def event_create(request):
    if request.method == 'POST':
        access_token = request.COOKIES['access_token']
        creator = User.objects.filter(access_token=access_token)[0]
        event =  Event(creator=creator)
        form = EventForm(request.POST, instance=event)
        event = form.instance
        tags = request.POST['tags_event']
        location_count = request.POST['location_count']
        location = ""
        for i in xrange(int(location_count)+1):
            location += request.POST['location' + str(i)] + " "
        event.location = location
        event.save()
        tags = tags.split()
        for tg in tags:
            tag = Tag(name=tg)
            tag.save()
            EventTag(event=event, tag=tag).save()

        return HttpResponseRedirect('/')
    else:
        form = EventForm()

    return render(request, 'meets/event.html', {
        'form': form, 'need_to_login':need_to_login}
    )

@login_required
def subscribe_event(request):
    e = Event.objects.filter(pk=request.POST.get('id'))
    EventUser.objects.get_or_create(user=request.user, event=e)
    return HttpResponseRedirect('/events/?id=' + e.pk)

def profile(request):
    need_to_login = not request.COOKIES.has_key('access_token')
    access_token = request.COOKIES['access_token']
    creator = User.objects.filter(access_token=access_token)[0]

    return render(request, 'meets/profile.html',
        {'u': creator, 'need_to_login':need_to_login}
    )

@csrf_exempt
@ajax
def jsons(request):
    E_NO_EVENT = {'error':'no_event',}
    E_OK = {'result': 'OK',}

    if settings.AJAX_DEBUG and not request.is_ajax():
        input = request.REQUEST
    else:
        input = json.loads(request.body)

    action = input.get('action')
    
    user = request.vk_user
    print user

    if 'oauth_vk' in request:
        return user


    if 'last_events' in action:
        offset = int(input['offset'])
        events = Event.objects.all()[offset:offset+20]
        return events

    if 'open_event' in action:
        id = int(input['id'])
        e = list(Event.objects.filter(pk=id))
        return e[0] if e else E_NO_EVENT

    if 'subscribe_event' in action:
        user = get_object_or_404(User, pk=1)
        id = int(input.get('id', 0))
        event = list(Event.objects.filter(pk=id))
        if not event:
            return E_NO_EVENT

        ev_us = Event.objects.get_or_create(user=user, event=event)
        
        return E_OK

    if 'cancel_event' in action:
        id = int(input['id'])
        EventUser.objects.filter(user=user, event__id=id).delete()

        return E_OK

def vk_login(request):
    VK_LINK = 'https://oauth.vk.com/access_token'
    response = redirect('/')

    if request.vk_user is not None:
        return response

    code = request.REQUEST.get('code')
    if not code:
        return response

    get = dict(
            client_id=settings.VK_APP_ID,
            client_secret=settings.VK_APP_SECRET,
            code=code,
            redirect_uri='http://%s/vk_login/' % request.get_host())
    print get

    link = VK_LINK + '?' + urlencode(get)
    print link
    resp, c = Http().request(link)

    dat = json.loads(c)
    user_id = dat['user_id']
    print user_id
    access_token = dat['access_token']
    expires_in = dat['expires_in']

    get = dict (
            user_id = user_id,
            fields = "photo_50",
            access_token = access_token
        )

    dat = vk.api('users.get', **get)['response'][0]

    fname = dat['first_name'].encode('utf-8')
    lname = dat['last_name'].encode('utf-8')
    photo = dat['photo_50'].encode('utf-8')

    response.set_cookie('fistname', fname, expires=expires_in)
    response.set_cookie('lastname', lname, expires=expires_in)
    response.set_cookie('access_token', access_token, expires=expires_in)


    User(
        vk_id = user_id,
        access_token = access_token,
        expires_in = expires_in,
        first_name = fname,
        last_name = lname,
        userpic = photo).save()

    return response


