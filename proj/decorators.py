from proj.utils import json_response

def ajax(f):
    def wrp(*args, **kwargs):
        return json_response(f(*args, **kwargs))

    return wrp
