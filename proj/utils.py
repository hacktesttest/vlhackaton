from datetime import date, datetime
from json import dumps as jdumps
from django.shortcuts import HttpResponse
from django.db.models.query import QuerySet
from django.template import Library

from meetfactory.models import SerializableModel

register = Library()

@register.filter
def checked(arg): 
    return 'true' if arg else 'false'

def json_response(j):
    def handle(obj):
        if isinstance(obj, datetime) or isinstance(obj, date):
            return obj.isoformat()
        elif isinstance(obj, SerializableModel):
            return obj.serialize()
        elif isinstance(obj, QuerySet):
            return list(obj)
        else:
            return None
    return HttpResponse(jdumps(j, default=handle))
