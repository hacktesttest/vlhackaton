from urllib import urlencode
from httplib2 import Http
from firebase_token_generator import create_token
import json
from proj.meetfactory.models import *
from django.shortcuts import render, redirect
from django.conf import settings

VK_LINK = 'https://oauth.vk.com/access_token'# + \
#    'client_id=' + settings.VK_APP_ID + '&' +\
#    'client_secret=' + settings.VK_APP_SECRET + '&' +\
#    'code=%s&' +\
#    'redirect_uri=localhost:8000/vk_login'
#    'redirect_uri=localhost:8000/vk_login'

#        (settings.VK_APP_ID, settings.VK_APP_SECRET,)


def vk_login(request):
    response = redirect('/meetfactory')
    code = request.REQUEST.get('code')
    if not code:
        return response

    get = dict(
            client_id=settings.VK_APP_ID,
            client_secret=settings.VK_APP_SECRET,
            code=code,
            redirect_uri='http://localhost:8000/vk_login')

    link = VK_LINK + '?' + urlencode(get)
    resp, c = Http().request(link)

    dat = json.loads(c)
    user_id = dat['user_id']
    access_token = dat['access_token']
    expires_in = dat['expires_in']

    get = dict (
            user_ids = user_id,
            fields = "photo_50",
        )

    link = "https://api.vk.com/method/users.get?" + urlencode(get) + "access_token=" + access_token

    resp, c = Http().request(link)
    print resp, c

    #User(user_id=user_id, access_token=access_token, expires_in=expires_in).save()

    return response

